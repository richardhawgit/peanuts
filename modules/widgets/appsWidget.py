import os

from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon
from PySide2.QtGui import QFont
from PySide2.QtCore import QSize
from PySide2.QtWidgets import QMenu
from PySide2.QtWidgets import QAction
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QLayout
from PySide2.QtWidgets import QToolButton
from PySide2.QtWidgets import QHBoxLayout
from PySide2.QtWidgets import QListWidgetItem


class main(QWidget):
    def __init__(self,datas,last_button = False):
        super(main,self).__init__()

        self.datas = datas
        self.last_button = last_button

        self.name = self.datas.get("name","")
        self.path = self.datas.get("path","")
        self.icon_path = self.datas.get("icon","")
        self.comments = self.datas.get("comments","No comments...")
        self.plugins = self.datas.get("plugins",[])
        self.environ = self.datas.get("environ",{})
        self.json_file = ""

        self.size = 45
        self.font_size = 14
        self.font_weight = 65
        self.button_style = None
        self.button_css = "QToolButton:hover:!pressed{background-color: mediumaquamarine;}"

        self.setup()
        self.build()
        self.createPopup()


    def setup(self):
        if os.environ["OS"] in ["nt"]:
            self.font_size-=5

        self.setStyleSheet("background-color: lightslategrey;")
        self.setFixedSize(self.font_size*3,self.font_size+6)
        self.setMinimumSize(self.font_size*3,self.font_size+6)

        if self.last_button:
            self.button_css = "QToolButton:hover:!pressed{background-color:pink;}"
            self.button_style = Qt.ToolButtonTextOnly
            self.font_weight = 88

            if os.name in ["nt"]:
                self.font_size += 1
            else:
                self.font_size += 4
        else:
            self.button_style = Qt.ToolButtonTextBesideIcon

        if self.last_button:
            return

        self.setToolTip("\n".join([self.path,self.comments]))


    def build(self):
        # button
        self.button = QToolButton()
        self.button.setStyleSheet(self.button_css)
        self.button.setFixedSize(self.size*3,self.size+6)
        self.button.setIconSize(QSize(self.size,self.size))
        self.button.setText(self.name.replace("-","\n"))

        if os.path.isfile(self.icon_path):
            self.icon = QIcon(os.path.join(os.path.dirname(__file__),self.icon_path))
            self.button.setIcon(self.icon)

            i_name_len = len(self.name)

            if i_name_len > 7:
                self.font_size = i_name_len*0.8
            elif i_name_len > 10:
                self.font_size = i_name_len*0.6
            elif i_name_len > 12:
                self.font_size = i_name_len*0.5
            elif i_name_len > 15:
                self.font_size = i_name_len*0.3
        else:
            self.font_weight = 70

        self.font = QFont()
        self.font.setPointSize(self.font_size)
        self.font.setWeight(self.font_weight)
        self.button.setToolButtonStyle(self.button_style)

        self.button.setFont(self.font)

        # layout
        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(5,5,5,5)
        self.layout.setMargin(0)
        self.layout.setSpacing(0)
        self.layout.setSizeConstraint(QLayout.SetFixedSize)
        self.layout.addWidget(self.button)
        self.layout.addStretch()
        self.setLayout(self.layout)

        # container
        self.container = QListWidgetItem()
        self.container.setSizeHint(self.sizeHint())


    def createPopup(self):
        if self.last_button:
            return

        self.edit_action = QAction("Edit Settings",self)
        self.kill_action = QAction("Remove App",self)

        self.popup = QMenu(self)
        self.popup.setStyleSheet("background-color:grey;")
        self.popup.addSeparator()
        self.popup.addAction(self.edit_action)
        self.popup.addAction(self.kill_action)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)


    def on_context_menu(self,point):
        self.popup.exec_(self.mapToGlobal(point))