from PySide2.QtCore import Qt
from PySide2.QtWidgets import QComboBox

from lib import qtUtils


class main(QComboBox):
    def __init__(self,menu_items,value,title=False):
        super(main,self).__init__()
        self.menu_items = menu_items
        self.value = value
        style = "QComboBox {background-color: lightgrey; border: 0px}"
        style+="QWidget {background-color: lightgrey;}"
        self.setFocusPolicy(Qt.StrongFocus)
        self.setStyleSheet(style)
        self.setFixedHeight(30)

        for mn in self.menu_items:
            if title:
                mn = mn.title()

            self.addItem(str(mn))

        if self.value is not None:
            qtUtils.comboBox.setValue(self,self.value)


    def wheelEvent(self,*args,**kwargs):
        return