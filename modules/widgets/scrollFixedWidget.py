from PySide2.QtCore import Qt
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QLayout
from PySide2.QtWidgets import QScrollArea
from PySide2.QtWidgets import QVBoxLayout


class main(QWidget):
    def __init__(self):
        super(main,self).__init__()
        self.create()


    def create(self):
        self.setMaximumHeight(16777215)

        self.scroll = QScrollArea()
        self.scroll.setWidgetResizable(True)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setStyleSheet("background-color: black; border: none;")

        self.layout = QVBoxLayout(self)
        self.layout.addStretch(True)
        self.scroll.setWidget(self)
