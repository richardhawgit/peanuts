from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QToolButton


class main(QToolButton):
    def __init__(self,label,icon,color):
        super(main,self).__init__()
        self.label = label
        self.icon = icon
        self.color = color

        self.create()


    def create(self):
        icon = QIcon(self.icon)
        #self.setStyleSheet("QToolButton:hover {background-color: mediumaquamarine}")
        self.setStyleSheet("QToolButton {{background-color: {}}}".format(self.color))
        self.setText(self.label)
        self.setMinimumHeight(25)
        self.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.setIcon(icon)