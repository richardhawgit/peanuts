from PySide2.QtCore import QSize
from PySide2.QtWidgets import QListView
from PySide2.QtWidgets import QSizePolicy
from PySide2.QtWidgets import QListWidget
from PySide2.QtWidgets import QAbstractItemView


class main(QListWidget):
    def __init__(self,sorting = False):
        super(main,self).__init__()
        self.sorting = sorting
        self.size = 45
        self.setup()


    def setup(self):
        self.setViewMode(self.IconMode)
        self.setResizeMode(self.Adjust)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setMovement(QListView.Static)

        self.setContentsMargins(0,0,0,0)
        self.setIconSize(QSize(self.size*3,self.size+6))
        self.setGridSize(QSize(self.size*3+15,self.size+6+10))
        self.setWrapping(True)
        self.setUniformItemSizes(True)
        self.setSizePolicy(QSizePolicy.Preferred,QSizePolicy.Preferred)
        self.setSortingEnabled(self.sorting)
        self.setStyleSheet("background-color: black; border: none;")