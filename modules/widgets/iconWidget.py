from PySide2.QtWidgets import QWidget, QToolButton, QVBoxLayout
from PySide2.QtCore import Qt, QSize


class main(QWidget):
    def __init__(self,name):
        super(main, self).__init__()

        self.name = name

        # controls
        self.button = QToolButton()
        self.button.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.button.setFixedSize(128,128)
        self.button.setIconSize(QSize(80,80))
        self.button.setText(name)

        # layout
        main_layout = QVBoxLayout()
        main_layout.setContentsMargins(0,0,0,0)
        main_layout.setSpacing(0)
        main_layout.addWidget(self.button)
        main_layout.addStretch()
        self.setLayout(main_layout)