from PySide2.QtCore import Qt
from PySide2.QtWidgets import QHeaderView
from PySide2.QtWidgets import QTableWidget

from lib.qtUtils import table as qtable


class main(QTableWidget):
    def __init__(self,rows,columns,list_data=[]):
        super(main,self).__init__()
        self.rows = rows
        self.columns = columns
        self.list_data = list_data

        self.build()
        qtable.populateList(self,self.list_data,2)


    def build(self):
        self.setRowCount(self.rows)
        self.setColumnCount(self.columns)
        self.setShowGrid(True)
        self.horizontalHeader().hide()
        self.setGridStyle(Qt.SolidLine)
        self.setAlternatingRowColors(True)
        self.setStyleSheet("background-color: lightgrey;")
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.verticalHeader().setStyleSheet("background-color:lightslategrey;")
