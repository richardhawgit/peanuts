import os
from PySide2.QtWidgets import QLineEdit


class main(QLineEdit):
    def __init__(self,info_string,is_link = True):
        super(main,self).__init__()
        self.info_string = info_string
        self.is_link = is_link

        self.setup()


    def setup(self):
        self.setText(str(self.info_string))
        self.setStyleSheet("background-color:lightgrey;")
        self.setMinimumHeight(30)
        self.setDragEnabled(True)


    def dragEnterEvent(self,event):
        data = event.mimeData()
        urls = data.urls()

        if (urls and urls[0].scheme() == "file"):
            event.acceptProposedAction()


    def dragMoveEvent(self,event):
        data = event.mimeData()
        urls = data.urls()

        if (urls and urls[0].scheme() == "file"):
            event.acceptProposedAction()


    def dropEvent(self,event):
        data = event.mimeData()
        urls = data.urls()

        if ( urls and urls[0].scheme() == "file"):
            filepath = str(urls[0].path())

            if not self.is_link or os.name in ["nt"]:
                filepath = os.path.normpath(filepath[1:])

            self.setText(filepath)