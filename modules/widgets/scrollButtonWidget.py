import os

from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon
from PySide2.QtGui import QFont
from PySide2.QtCore import QSize
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QLayout
from PySide2.QtWidgets import QToolButton
from PySide2.QtWidgets import QHBoxLayout
from PySide2.QtWidgets import QListWidgetItem


class main(QWidget):
    def __init__(self,name,icon_path):
        super(main,self).__init__()

        self.name = name.title()
        self.icon_path = icon_path
        self.size = 45
        self.font_size = 14
        self.font_weight = 65
        self.button_style = None
        self.button_css = "QToolButton:hover:!pressed{background-color: mediumaquamarine;}"

        self.setup()
        self.build()


    def setup(self):
        if os.environ["OS"] in ["nt"]:
            self.font_size-=5

        self.setStyleSheet("background-color: lightslategrey;")
        self.setFixedSize(self.font_size*3,self.font_size+6)
        self.setMinimumSize(self.font_size*3,self.font_size+6)
        self.button_style = Qt.ToolButtonTextBesideIcon


    def build(self):
        # button
        self.button = QToolButton()
        self.button.setStyleSheet(self.button_css)
        self.button.setFixedSize(self.size*3,self.size+6)
        self.button.setIconSize(QSize(self.size,self.size))
        self.button.setText(self.name.replace("-","\n"))

        if os.path.isfile(self.icon_path):
            self.icon = QIcon(os.path.join(os.path.dirname(__file__),self.icon_path))
            self.button.setIcon(self.icon)

            i_name_len = len(self.name)

            if i_name_len > 7:
                self.font_size = i_name_len*0.8
            elif i_name_len > 10:
                self.font_size = i_name_len*0.6
            elif i_name_len > 12:
                self.font_size = i_name_len*0.5
            elif i_name_len > 15:
                self.font_size = i_name_len*0.3
        else:
            self.font_weight = 70

        self.font = QFont()
        self.font.setPointSize(self.font_size)
        self.font.setWeight(self.font_weight)
        self.button.setToolButtonStyle(self.button_style)

        self.button.setFont(self.font)

        # layout
        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(5,5,5,5)
        self.layout.setMargin(0)
        self.layout.setSpacing(0)
        self.layout.setSizeConstraint(QLayout.SetFixedSize)
        self.layout.addWidget(self.button)
        self.layout.addStretch()
        self.setLayout(self.layout)

        # container
        self.container = QListWidgetItem()
        self.container.setSizeHint(self.sizeHint())