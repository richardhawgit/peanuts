import os

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QLineEdit
from PySide2.QtWidgets import QFileDialog
from PySide2.QtWidgets import QToolButton
from PySide2.QtWidgets import QHBoxLayout

import textLineWidget as text_wd


class main(QWidget):
    def __init__(self,label_text,file_path,filters = "All files (*)"):
        super(main,self).__init__()
        self.label_text = label_text
        self.file_path = file_path
        self.filters = filters

        self.setFixedHeight(30)
        self.build()
        self.arrange()


    def build(self):
        self.label = QLabel(self.label_text)
        self.label.setStyleSheet("color:lightgrey;font-weight:bold;")
        self.label.setAlignment(Qt.AlignVCenter)

        self.text = text_wd.main(self.file_path)
        self.text.setFixedHeight(30)
        self.text.setStyleSheet("background-color:lightgrey;")

        self.button = QToolButton()
        self.button.setText("<<")
        self.button.setFixedSize(30,30)
        self.button.setStyleSheet("background-color:lightslategrey;font-weight:bold")
        self.button.clicked.connect(self.buttonClickedCmd)

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setMargin(0)


    def arrange(self):
        for wd in [self.label,self.text,self.button]:
            self.layout.addWidget(wd)

        self.setLayout(self.layout)


    def buttonClickedCmd(self):
        app_loc = os.path.expanduser("~/Applications")

        app_file,_ = QFileDialog.getOpenFileName(self,
                                                 self.tr("Select application:"),
                                                 self.tr(app_loc),
                                                 self.tr(self.filters))
        self.text.setText(app_file)