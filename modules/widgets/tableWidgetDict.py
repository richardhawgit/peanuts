from PySide2.QtWidgets import QHeaderView

import tableWidgetRows
from lib.qtUtils import table as qtable


class main(tableWidgetRows.main):
    def __init__(self,rows,dict_data):
        super(main,self).__init__(rows,2)

        self.horizontalHeader().show()
        self.horizontalHeader().setStyleSheet("QHeaderView::section {background-color:lightslategrey;}")
        self.setHorizontalHeaderLabels(["Name","Value"])
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
        self.setColumnWidth(0,150)
        self.horizontalHeader().setStretchLastSection(True)

        qtable.populateDict(self,dict_data)