from os import environ

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QHeaderView
from PySide2.QtWidgets import QTableWidget
from PySide2.QtWidgets import QTableWidgetItem

from lib.qtUtils import table as qtable


class main(QTableWidget):
    def __init__(self,list_data,columns=7):
        super(main,self).__init__()
        self.list_data = list_data
        self.columns = columns

        try:
            self.create()
        except:
            self.create()

        qtable.populateList(self,self.list_data,1)


    def create(self):
        os_height = 50
        self.setRowCount(1)
        self.setColumnCount(self.columns)

        self.setShowGrid(True)
        self.setGridStyle(Qt.SolidLine)
        self.setStyleSheet("background-color:lightgrey;")

        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.horizontalHeader().setStyleSheet("QHeaderView::section {background-color:lightslategrey;}")
        self.verticalHeader().hide()

        if environ["OS"] not in ["nt"]:
            self.horizontalHeader().setMaximumHeight(20)
        else:
            os_height = 60

        self.setMaximumHeight(os_height)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)