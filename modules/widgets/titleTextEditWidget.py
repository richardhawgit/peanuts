import os

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QLineEdit
from PySide2.QtWidgets import QHBoxLayout


class main(QWidget):
    def __init__(self,label_text,file_path):
        super(main,self).__init__()
        self.label_text = label_text
        self.file_path = file_path

        self.setFixedHeight(30)

        self.label = QLabel(self.label_text)
        self.label.setStyleSheet("color:lightgrey;font-weight:bold;")
        self.label.setAlignment(Qt.AlignVCenter)

        self.text = QLineEdit(self.file_path)
        self.text.setFixedHeight(30)
        self.text.setStyleSheet("background-color:lightgrey;")

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setMargin(0)

        for wd in [self.label,self.text]:
            self.layout.addWidget(wd)

        self.setLayout(self.layout)