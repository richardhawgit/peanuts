from PySide2.QtWidgets import QToolButton
from PySide2.QtCore import QSize
from PySide2.QtGui import QIcon
from PySide2.QtGui import QFont


class main(QToolButton):
    def __init__(self,name,icon_path,size = 45,border = 0):
        super(main, self).__init__()
        self.name = name.center(8," ")
        self.icon_path = icon_path
        self.icon = None
        self.size = size
        self.border = border

        self.setup()


    def setup(self):
        self.setFixedSize(self.size+self.border,self.size+self.border)
        self.setIconSize(QSize(self.size,self.size))

        self.icon = QIcon(self.icon_path)
        self.setIcon(self.icon)

        self.setText(self.name)
        self.setStyleSheet("QToolButton{background-color: slategrey;}")

        font = QFont()
        font.setWeight(88)
        self.setFont(font)
