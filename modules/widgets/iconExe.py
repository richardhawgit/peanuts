from os.path import dirname as dname

from PySide2.QtGui import QIcon
from PySide2.QtCore import QFileInfo
from PySide2.QtWidgets import QFileSystemModel
from PySide2.QtWidgets import QFileIconProvider


class main(QIcon):
    def __init__(self,path,size = 45):
        super(main,self).__init__()
        self.path = path
        self.size = size
        self.build()


    def build(self):
        exe_info = QFileInfo(self.path)
        fs_model = QFileSystemModel()
        fs_model.setRootPath(exe_info.path())
        fs_model.setRootPath(dname(self.path))
        exe_prov = fs_model.iconProvider()
        self.swap(exe_prov.icon(self.path))
