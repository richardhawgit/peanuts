from os.path import isfile as is_file

from PySide2.QtWidgets import QToolButton
from PySide2.QtWidgets import QAction
from PySide2.QtWidgets import QMenu
from PySide2.QtGui import QPixmap
from PySide2.QtCore import QSize
from PySide2.QtGui import QFont
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt


class main(QToolButton):
    def __init__(self,name,w,path = ""):
        super(main,self).__init__()
        self.name = name
        self.w = w
        self.path = path

        self.create()
        self.createPopup()


    def create(self):
        styler = "QToolButton{background-color:slategrey;}"
        font_size = 10
        font_weight = 88
        nice_name = self.name

        if is_file(self.path):
            self.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
            self.pmap = QPixmap(self.path)
            self.icon = QIcon(self.pmap)
            self.setIcon(self.icon)
            self.setIconSize(QSize(self.w-15,self.w-15))
        else:
            self.setToolButtonStyle(Qt.ToolButtonTextOnly)
            styler = "QToolButton{background-color:slategrey;}"
            font_size = 12
            nice_name = nice_name.replace(" ","\n")

        font = QFont()
        font.setPointSize(font_size)
        font.setWeight(font_weight)
        self.setFont(font)

        self.setText(nice_name)
        self.setStyleSheet(styler)
        self.setFixedSize(self.w-4,self.w+10)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)


    def createPopup(self):
        browse_action = QAction("Browse Root",self)
        browse_action.triggered.connect(self.browseCmd)

        self.popup = QMenu(self)
        self.popup.setStyleSheet("background-color:grey;")
        self.popup.addSeparator()
        self.popup.addAction(browse_action)


    def browseCmd(self):
        print self


    def on_context_menu(self,point):
        self.popup.exec_(self.mapToGlobal(point))
