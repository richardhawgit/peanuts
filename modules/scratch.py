#project_keys = [{"name":"project_globals","value":None},
                #{"name":"project_name","value":"PROJECT_NAME"},
                #{"name":"project_code","value":"PROJ"},
                #{"name":"operating_system","value":("windows","mac","linux")},
                #{"name":"framerate","value":24},
                #{"name":"global_scale","value":("meters","centimeters","millimeters")},
                #{"name":"render_width","value":1920},
                #{"name":"render_height","value":1080},
                #{"name":"color_space","value":("Arri LogC","camera Rec 709","Sony SLog2","Log film scan (ADX)","Log-to-Lin (cineon)","Log-to-Lin (jzp)","Raw","ACES2065-1","ACEScg","scene-linear CIE XYZ","scene-linear DCI-P3","scene-linear Rec 2020","scene-linear Rec 709/sRGB","gamma 1.8 Rec 709","gamma 2.2 Rec 709","gamma 2.4 Rec 709 (video)","sRGB")},
                #{"name":"project_root","value":"C:\dummy\PROJ"}]

#assets_keys = [{"name":"assets_parameters","":None},
               #{"name":"assets_types","value":["char","env","prop","mech"]},
               #{"name":"assets_steps","value":["modeling","rigging","shading"]},
               #{"name":"assets_lod","value":["low","mid","high","render"]},
               #{"name":"assets_root","value":"{PROJECT_ROOT}/assets"}]

#shots_keys = [{"name":"shots_parameters","value":None},
              #{"name":"shots_steps","value":["layout","animation","lighting","effects","cfx","comp"]},
              #{"name":"shots_root","value":"{PROJECT_ROOT}/shots"},
              #{"name":"data_root","value":"{PROJECT_ROOT}/data"},
              #{"name":"render_root","value":"{PROJECT_ROOT}/render"},
              #{"name":"tier_names","value":["season","episode","sequence","shot"]},
              #{"name":"tier_patterns","value":["sn##","ep##","seq##","sh###"]}]

user_keys = [{"name":"(Saved to Local Prefs)","value":None},
             {"name":"user_name","value":"UserName"},
             {"name":"user_email","value":"UserName@email.com"},
             {"name":"admin_email","value":"admin@email.com"},
             {"name":"department","value":"modeling"},
             {"name":"team","value":"assets"}]

from lib import ioUtils
ioUtils.jsonUtils.write(r"C:\repo\peanuts\modules\appManager\config\defaults\user_keys.json",user_keys)
