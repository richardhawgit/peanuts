import sys, os, json, shutil
from pprint import pprint
from traceback import print_exc

from PySide2.QtCore import QSize
from PySide2.QtGui import QPixmap


class picUtils():
    @staticmethod
    def harvestIcon(icon_path,des_path):
        dirUtils.createFileParent(des_path)

        if not os.path.isfile(icon_path):
            return

        shutil.copyfile(icon_path,des_path)
        return True


    @staticmethod
    def harvestIconExe(qicon_pointer,icon_path):
        dirUtils.createFileParent(icon_path)

        act_size = qicon_pointer.actualSize(QSize(35,35))
        pixmap = QPixmap(qicon_pointer.pixmap(act_size))
        pixmap.save(icon_path,"PNG")
        return True


class dirUtils():
    @staticmethod
    def createFileParent(file_path):
        file_root = os.path.dirname(file_path)

        if os.path.exists(file_root):
            return True

        try:
            os.makedirs(file_root)
            print("Created new folder: {}".format(file_root))
        except Exception as err:
            print(str(err))
            print("Failed to create folder: {}".format(file_root))
            return

        return True


    @staticmethod
    def archive(dir_path,out_zip,delete_orig = False):
        dirUtils.createFileParent(out_zip)

        try:
            shutil.make_archive(out_zip,"zip",dir_path)
        except Exception as err:
            print(err)
            print_exc()
            return

        if delete_orig:
            try:
                shutil.rmtree(dir_path)
            except Exception as err:
                print(err)
                print_exc()
                return

        return True


class jsonUtils():
    @staticmethod
    def write(filepath,data):
        dirUtils.createFileParent(filepath)

        try:
            with open(filepath, "w") as file_path:
                json.dump(data, file_path, indent = 4)
        except Exception as err:
            print(str(err))
            return

        return True


    @staticmethod
    def read(file_path):
        datas = None

        if not os.path.exists(file_path):
            print("No json file found: {}".format(file_path))
            return

        try:
            with open(file_path) as json_buffer:
                datas = json.load(json_buffer)
        except Exception as err:
            print(str(err))
            print("Failed to read: %s".format(file_path))
            return

        return datas


    @classmethod
    def update(self,file_path,update_key,update_datas):
        update_json = None
        current_data = None

        # if no file
        if not os.path.exists(file_path):
            update_json = {update_key : update_datas}

            if not self.write(file_path,update_json):
                print("Failed to write updated json.")
                pprint(update_json)
                return

            return True

        # read existing and update
        current_data = self.read(file_path)

        if not current_data:
            current_data = {}

        current_data[update_key] = update_datas

        if not self.write(file_path,current_data):
            print("Failed to update json: {}".format(file_path))
            pprint(current_data)
            return

        return True


    @classmethod
    def inject(self,file_path,new_data):
        load_buffer = None
        dirUtils.createFileParent(file_path)

        if not os.path.exists(file_path):
            if not self.write(file_path,new_data):
                print("Failed to write updated json.")
                pprint(new_data)
                return

            return True

        with open(file_path) as json_buffer:
            load_buffer = json.load(json_buffer)

        load_buffer.update(new_data)

        with open(file_path, "w") as json_buffer:
            json.dump(load_buffer, json_buffer, indent = 4)

        return True


class fileUtils():
    @staticmethod
    def copy(source_file,target_file):
        dirUtils.createFileParent(target_file)

        try:
            shutil.copy(source_file,target_file)
        except Exception as err:
            print(err)
            print_exc()
            return

        return True