from traceback import print_exc

from PySide2.QtWidgets import QTableWidgetItem
from PySide2.QtWidgets import QMessageBox
from PySide2.QtCore import Qt


class widgets():
    @staticmethod
    def selectionUpdate(midget,all_midgets,color1="slategrey",color2="mediumaquamarine"):
        for mdg in all_midgets:
            mdg.setStyleSheet("background-color:{};".format(color1))

        midget.setStyleSheet("background-color:{};".format(color2))


class scrolls():
    @staticmethod
    def add(scroll,special_widget,cmd = None):
        scroll.insertItem(scroll.count(),special_widget.container)
        scroll.setItemWidget(special_widget.container,special_widget)

        if cmd:
            special_widget.button.clicked.connect(cmd)


class layouts():
    @staticmethod
    def clearGrid(grid_layout):
        for itm in reversed(range(grid_layout.count())):
            gidget = grid_layout.itemAt(itm).widget()

            if gidget is None:
                grid_layout.removeItem(grid_layout.itemAt(itm))
                continue

            try:
                gidget.deleteLater()
            except Exception as err:
                print err
                print_exc()
                return


class table():
    @staticmethod
    def populateList(widget,items,mode):
        out = []

        for it in items:
            table_item = QTableWidgetItem(str(it))
            table_item.setTextAlignment(Qt.AlignVCenter)

            if mode == 1:
                widget.setItem(0, items.index(it), table_item)
            elif mode == 2:
                widget.setItem(items.index(it), 0, table_item)

            out.append(table_item)

        return out


    @staticmethod
    def populateDict(widget,dict_data):
        out = []
        counter = 0

        for dd in dict_data:
            key_item = QTableWidgetItem(str(dd))
            data_item = QTableWidgetItem(str(dict_data[dd]))

            widget.setItem(counter,0,key_item)
            widget.setItem(counter,1,data_item)

            out.append([key_item,data_item])
            counter+=1

        return out


    @staticmethod
    def itemsList(widget,pointer = False):
        out = []

        for rw in range(widget.rowCount()):
            for cl in range(widget.columnCount()):
                witem = widget.item(rw,cl)

                if witem is None:
                    continue

                if pointer:
                    out.append(witem)
                else:
                    out.append(str(witem.text()))

        return out


    @staticmethod
    def itemsDict(widget,pointer = False):
        out = {}

        for rw in range(widget.rowCount()):
            w_key = widget.item(rw,0)
            w_value = widget.item(rw,1)

            if w_key is None or w_value is None:
                continue

            if pointer:
                out[w_key.text()] = w_value
            else:
                out[w_key.text()] = w_value.text()

        return out


class comboBox():
    @staticmethod
    def setValue(combo_widget,value):
        item_index = combo_widget.findText(value)

        if item_index != -1:
            combo_widget.setCurrentIndex(item_index)


class dialogs():
    @staticmethod
    def yesNo(tit,msg):
        answer = QMessageBox.question(None,
                                      tit,
                                      msg,
                                      QMessageBox.Yes,
                                      QMessageBox.No)

        if answer == QMessageBox.Yes:
            return True
        else:
            return False