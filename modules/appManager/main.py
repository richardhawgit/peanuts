import os,sys,glob
from pprint import pprint
from functools import partial
from traceback import print_exc

from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QDialog
from PySide2.QtWidgets import QTabWidget
from PySide2.QtWidgets import QVBoxLayout
from PySide2.QtWidgets import QHBoxLayout
from PySide2.QtWidgets import QPushButton
from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QListWidgetItem

top_path = os.path.abspath(os.path.join(__file__,"../.."))
if not top_path in sys.path:
    sys.path.append(top_path)

from widgets import iconExe
from widgets import optionsIconWidget
from widgets import scrollWidget as scroll_wd
from widgets import tableWidgetRows as rows_wd
from widgets import tableWidgetDict as dict_wd
from widgets import fileTextEditWidget as file_wd
from widgets import titleTextEditWidget as titTxt_wd

from lib import qtUtils
from lib import ioUtils

debug = False
current_os = os.name


class main(QDialog):
    def __init__(self, app_data = None):
        super(main,self).__init__()

        self.app_data = app_data
        self.icon_scroll = None
        self.icons_list = []
        self.current_widget = None
        self.blank = os.path.join(os.path.dirname(__file__),"icons","app.png")
        self.setStyleSheet("background-color: rgb(35,35,35);")
        self.label_style = "color:lightgrey;font-weight:bold;"
        self.setWindowTitle("App Manager")
        self.root = ""
        self.project_name = ""

        self.name = ""
        self.path = ""
        self.comments = ""
        self.icon_path = self.blank
        self.plugins = []
        self.env_vars = {}

        self.initVars()
        self.build()
        self.arrange()
        self.populateIconList()

        self.resize(600,300)
        self.setModal(True)
        self.show()
        self.exec_()


    def initVars(self):
        self.root = os.environ.get("PEANUTS_ROOT",None)
        self.project_name = os.environ.get("PROJECT_CODE",None)

        if self.app_data is None:
            return

        self.name = self.app_data.get("name","APP_NAME")
        self.path = os.path.normpath(self.app_data.get("path",""))
        self.comments = self.app_data.get("comment","")
        self.icon_path = os.path.normpath(self.app_data.get("icon",self.blank))
        self.plugins = self.app_data.get("plugins",[])
        self.env_vars = self.app_data.get("environ",[])


    def build(self):
        ikon = QIcon("logo.png")
        self.setWindowIcon(ikon)

        self.app_name_text = titTxt_wd.main("App Name:",self.name)
        self.app_name_text.label.setFixedWidth(100)

        self.app_comment_text = titTxt_wd.main("Comment:",self.comments)
        self.app_comment_text.label.setFixedWidth(100)

        self.app_path_text = file_wd.main("App Path:",self.path,"Programs (*.exe;*.app)")
        self.app_path_text.label.setFixedWidth(100)
        self.app_path_text.text.setText(self.path)
        self.app_path_text.text.textChanged.connect(self.appChangedCmd)

        self.tabs = QTabWidget()
        self.tabs.setStyleSheet("background-color: transparent;")

        self.plugins_list = rows_wd.main(50,1,self.plugins)
        self.env_list = dict_wd.main(50,self.env_vars)

        self.button = QPushButton("Add/Edit Application")
        self.button.setStyleSheet("background-color: slategrey;")
        self.button.clicked.connect(self.editAppCmd)


    def arrange(self):
        self.main_layout = QVBoxLayout(self)
        self.main_layout.setMargin(10)
        self.icon_layout = QVBoxLayout()

        self.main_layout.addWidget(self.app_name_text)
        self.main_layout.addWidget(self.app_comment_text)
        self.main_layout.addWidget(self.app_path_text)
        self.main_layout.addLayout(self.icon_layout)
        self.main_layout.addWidget(self.tabs)
        self.main_layout.addWidget(self.button)
        self.main_layout.setContentsMargins(10,10,10,30)

        self.tabs.addTab(self.env_list,"Environment Variables")
        self.tabs.addTab(self.plugins_list,"Official Plugins List")


    def appChangedCmd(self):
        self.populateIconList()

        dummy_name = os.path.basename(self.path)
        self.app_name_text.text.setText(dummy_name.split(".")[0].title())

        self.comments = dummy_name
        self.app_comment_text.text.setText(self.comments)


    def populateIconList(self):
        def addToIcons(icon_path):
            new_icon = iconWidget(icon_path)
            new_icon.clicked.connect(partial(self.iconCmd,new_icon))
            self.icon_scroll.insertItem(self.icon_scroll.count(),new_icon.container)
            self.icon_scroll.setItemWidget(new_icon.container,new_icon)
            self.icons_list.append(new_icon)
            return new_icon


        def addToIconNt(exe_path):
            exe_widget = iconWidget(self.blank)

            if os.path.isfile(exe_path):
                exe_icon = iconExe.main(exe_path)
                exe_widget.setIcon(exe_icon)

            self.icon_scroll.insertItem(self.icon_scroll.count(),exe_widget.container)
            self.icon_scroll.setItemWidget(exe_widget.container,exe_widget)
            exe_widget.clicked.connect(partial(self.iconCmd,exe_widget))
            self.icons_list.append(exe_widget)
            return exe_widget


        if not self.icon_scroll is None:
            self.icon_scroll.deleteLater()

        self.icons_list = []
        self.icon_scroll = scroll_wd.main()
        self.icon_scroll.setFixedHeight(75)
        self.icon_scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.icon_layout.addWidget(self.icon_scroll)

        self.path = os.path.normpath(self.app_path_text.text.text())
        all_icons = []

        if os.path.isfile(self.icon_path):
            all_icons.append(self.icon_path)
        else:
            all_icons.append(self.blank)

        # for windows
        if current_os in ["nt"]:
            nt_widget = addToIconNt(self.path)
            nt_widget.click()
            return

        # for mac
        if not os.path.exists(self.path):
            blank_icon = addToIcons(self.blank)
            blank_icon.click()
            self.icons_list = [blank_icon]
            return

        icons_root = [self.path,"Contents","Resources"]

        for ext in ["*.ico","*.icns"]:
            app_res = os.path.join(*icons_root+[ext])
            all_icons+=glob.glob(app_res)

        for icb in all_icons:
            addToIcons(icb)

        if len(self.icons_list) > 1:
            self.icons_list[1].click()


    def iconListCmd(self):
        self.icon_list.currentItem().text()


    def iconCmd(self,widget):
        self.current_widget = widget
        qtUtils.widgets.selectionUpdate(widget,self.icons_list)


    def editAppCmd(self):
        if not qtUtils.dialogs.yesNo("App Manager","Add/Edit application?"):
            return

        self.name = self.app_name_text.text.text()
        self.path = self.app_path_text.text.text()
        self.comments = self.app_comment_text.text.text()
        self.plugins = qtUtils.table.itemsList(self.plugins_list)
        self.env_vars = qtUtils.table.itemsDict(self.env_list)
        out_icon = ""
        icon_file = "{}.png".format(os.path.basename(self.path.rstrip("/").rstrip("\\")).split(".")[0])

        if debug:
            print self.name
            print self.path
            print self.comments
            print self.env_vars
            print self.plugins
            return

        if not os.path.exists(self.path):
            print "Not a valid app: {}".format(self.path)
            return

        try:
            out_icon = os.path.join(os.path.dirname(top_path),
                                    "projects",
                                    self.project_name,
                                    "apps",
                                    "icons",
                                    icon_file)
        except Exception as err:
            print err
            print_exc()
            print top_path,self.project_name,icon_file
            print "Failed to parse icon path..."
            return

        tmp_icon = iconExe.main(self.path)
        ioUtils.picUtils.harvestIconExe(tmp_icon,out_icon)

        if not os.path.isfile(out_icon):
            out_icon = self.blank

        app_json = os.path.join(self.root,
                                "projects",
                                self.project_name,
                                "apps",
                                "{}.json".format(self.name))

        app_keys = {"name":self.name,
                    "path":self.path,
                    "icon":out_icon,
                    "comments":self.comments,
                    "plugins":self.plugins,
                    "environ":self.env_vars}

        ioUtils.jsonUtils.write(app_json,app_keys)
        pprint(app_keys)
        self.close()


class iconWidget(optionsIconWidget.main):
    def __init__(self,path):
        self.path = os.path.normpath(path)
        self.name = os.path.basename(path).partition(".")[0]
        size = 44

        if current_os in ["nt"]:
            size = 40

        super(iconWidget,self).__init__(self.name,self.path,size,8)

        self.container = QListWidgetItem()
        self.container.setSizeHint(self.sizeHint())


if __name__ == "__main__":
    debug = True
    top_app = QApplication(sys.argv)
    ui = main(None)
    top_app.exec_()
    sys.exit(0)