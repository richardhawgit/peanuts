import os,sys,subprocess,glob
from functools import partial
from traceback import print_exc

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QSpacerItem
from PySide2.QtWidgets import QSizePolicy
from PySide2.QtWidgets import QApplication

top_path = os.path.abspath(os.path.join(__file__,"../.."))
if top_path not in sys.path:
    sys.path.append(top_path)

from widgets import appsWidget as app_wd
from widgets import comboWidget as combo_wd
from widgets import textLineWidget as txt_wd
from widgets import scrollWidget as scroll_wd
from widgets import projectsButtonWidget as proj_wd
from widgets import tableWidgetColumns as columns_wd

from lib import qtUtils
from lib import ioUtils
import interface as ui
import operations as ops
from appManager import main as app_man


os.environ["OS"] = os.name
os.environ["PEANUTS_ROOT"] = os.path.abspath(os.path.join(os.getcwd(),"../.."))
os.environ["LOCAL_ROOT"] = os.path.abspath(os.path.join(os.path.expanduser("~"),"peanuts"))
os.environ["PROJECT_CODE"] = ""


class run(QApplication):
    def __init__(self):
        super(run,self).__init__(sys.argv)
        self.all_proj_widgets = []
        self.all_apps_widget = []
        self.apps_parent_widget = None
        self.current_proj_btn = None
        self.current_proj_code = ""
        self.user_name = ""
        self.current_app_btn = None
        self.projects_root = os.path.join(os.environ["PEANUTS_ROOT"],"projects")
        self.defaults_root = os.path.join(os.getcwd(),"config","defaults")

        self.ui = ui.build()
        self.ui.show()
        self.linkCommands()
        self.initConfigs()
        self.ui.options_widgets[0].click()
        self.selectFirstProject()


    def linkCommands(self):
        self.ui.settings_user_radio.toggled.connect(self.settingsRadioCmd)
        self.ui.settings_project_radio1.toggled.connect(self.settingsRadioCmd)
        self.ui.settings_project_radio2.toggled.connect(self.settingsRadioCmd)

        for bt in self.ui.options_widgets:
            bt.clicked.connect(partial(self.optionClickedCmd,
                                       self.ui.options_widgets.index(bt)))

        self.ui.settings_save_btn.clicked.connect(self.saveEditSettingsCmd)
        self.ui.settings_kill_btn.clicked.connect(self.archiveProjectCmd)


    def populateProjects(self,proj_list):
        self.all_proj_widgets = []
        qtUtils.layouts.clearGrid(self.ui.proj_layout)

        proj_spacer = QSpacerItem(1,
                                  self.ui.ultimate,
                                  QSizePolicy.Minimum,
                                  QSizePolicy.Expanding)

        for pj in proj_list:
            pj_thumb = os.path.join(self.projects_root,pj,"thumb.png")
            pwid = proj_wd.main(pj,self.ui.proj_w-25,pj_thumb)
            pwid.clicked.connect(partial(self.projectClickedCmd,pwid))
            self.ui.proj_layout.addWidget(pwid,0,Qt.AlignCenter)
            self.all_proj_widgets.append(pwid)

        self.ui.proj_layout.addItem(proj_spacer)


    def populateApps(self):
        self.all_apps_widget = []

        if not self.apps_parent_widget is None:
            self.apps_parent_widget.deleteLater()

        self.apps_parent_widget = scroll_wd.main()
        self.ui.apps_layout.addWidget(self.apps_parent_widget)

        apps_dir = os.path.join(os.getenv("PEANUTS_ROOT"),
                                "projects",
                                self.current_proj_code,
                                "apps")

        for app_json in glob.glob(os.path.join(apps_dir,"*.json")):
            app_data = ioUtils.jsonUtils.read(app_json)

            if app_data is None:
                continue

            try:
                a_wd = app_wd.main(app_data)
            except Exception as err:
                print err
                print app_json
                print_exc()
                continue

            a_wd.json_file = app_json
            a_wd.edit_action.triggered.connect(partial(self.editAppCmd,a_wd))
            a_wd.kill_action.triggered.connect(partial(self.killAppCmd,a_wd))

            qtUtils.scrolls.add(self.apps_parent_widget,
                                a_wd,
                                partial(self.appClickedCmd,a_wd))

            self.all_apps_widget.append(a_wd)

        new_app = app_wd.main({"name":"Add New App"},True)
        qtUtils.scrolls.add(self.apps_parent_widget,
                            new_app,
                            self.addAppCmd)


    def initConfigs(self):
        projects_list = [p for p in os.listdir(self.projects_root)\
                         if os.path.isdir(os.path.join(self.projects_root,p))\
                         and not p.startswith(".")]

        # create defualts
        if not os.path.exists(self.projects_root):
            os.makedirs(self.projects_root)

        # populate projects
        self.populateProjects(projects_list)


    def selectFirstProject(self):
        if len(self.all_proj_widgets)>0:
            self.all_proj_widgets[0].click()


    def populateSettings(self,setts_widget,setts_keys):
        all_edits = []
        qtUtils.layouts.clearGrid(setts_widget)

        for st in setts_keys:
            st_name = st.get("name","no_name")
            st_value = st.get("value",None)
            st_predefined = st.get("predefined",None)
            st_type = type(st_value)
            st_index = setts_keys.index(st)
            st_title = st_name.title().replace("_"," ")+":"

            l_label = QLabel(st_title)
            l_label.setStyleSheet(self.ui.settings_label_style)
            l_wd = None

            if st_value is None:
                l_label.setMinimumHeight(35)
                l_label.setAlignment(Qt.AlignBottom)
                l_label.setText(st_title.rstrip(":"))
                setts_widget.addWidget(l_label,st_index,1)
                continue

            if st_type in [str,unicode,int,float] and st_predefined is None:
                l_wd = txt_wd.main(st_value)
            elif st_type in [str,unicode] and type(st_predefined) is list:
                l_wd = combo_wd.main(st_predefined,st_value)
            elif st_type is list and st_predefined is None:
                l_wd = columns_wd.main(st_value)

            if l_wd is None:
                continue

            setts_widget.addWidget(l_label,st_index,0)
            setts_widget.addWidget(l_wd,st_index,1)
            all_edits.append(l_wd)

        return all_edits


    def buildProjSettings(self,project_code):
        configs_root = os.path.join(self.defaults_root,os.getenv("OS"))

        if project_code is not None:
            configs_root = os.path.join(self.projects_root,project_code,"config")

        jsons = glob.glob(os.path.join(configs_root,"[0-9]_*_*.json"))
        settings = []

        for settings_json in jsons:
            settinds_data = ioUtils.jsonUtils.read(settings_json)
            settings+=settinds_data

        self.populateSettings(self.ui.settings_project_grid,settings)


    def buildUserSettings(self):
        user_keys = []

        dummy_local = os.path.join(os.environ["LOCAL_ROOT"],
                                   self.current_proj_code)

        if not os.path.isdir(dummy_local):
            print "No locals found: {}".format(dummy_local)
            dummy_local = self.defaults_root

        user_patt = os.path.join(dummy_local,"*_settings.json")

        for js in glob.glob(user_patt):
            user_keys+=ioUtils.jsonUtils.read(js)

        self.populateSettings(self.ui.settings_user_grid,user_keys)

        for uk in user_keys:
            if uk["name"] == "user_name":
                self.user_name = uk["value"]
                break

        user_font = 20
        user_len = len(self.user_name)

        if user_len > 10 and user_len < 15:
            user_font = 18
        elif user_len > 15 and user_len < 20:
            user_font = 16
        elif user_len > 20 and user_len < 25:
            user_font = 12
        elif user_len > 25:
            user_font = 10

        user_style = "font-size: {}px; color: grey".format(str(user_font))

        self.ui.user_label.setText(self.user_name)
        self.ui.user_label.setStyleSheet(user_style)


    # commands
    def optionClickedCmd(self,mode):
        self.ui.stack.setCurrentWidget(self.ui.all_stacks[mode])
        qtUtils.widgets.selectionUpdate(self.ui.options_widgets[mode],
                                        self.ui.options_widgets)
        niceW = self.ui.winW*3+10

        if mode == 2:
            if self.ui.width() < 600:
                self.ui.resize(niceW,600)
            self.ui.setMinimumWidth(niceW)
        else:
            self.ui.setMinimumWidth(self.ui.winW)


    def settingsRadioCmd(self):
        proj_tag = self.current_proj_code
        if self.ui.settings_user_radio.isChecked():
            self.ui.settings_stack.setCurrentWidget(self.ui.settings_user_widget.scroll)
            self.ui.settings_kill_btn.hide()

        if self.ui.settings_project_radio1.isChecked():
            self.ui.settings_stack.setCurrentWidget(self.ui.settings_project_widget.scroll)
            self.ui.settings_kill_btn.show()

        if self.ui.settings_project_radio2.isChecked():
            self.ui.settings_stack.setCurrentWidget(self.ui.settings_project_widget.scroll)
            self.ui.settings_kill_btn.hide()
            proj_tag = None

        self.buildProjSettings(proj_tag)


    def projectClickedCmd(self,pidget):
        qtUtils.widgets.selectionUpdate(pidget,self.all_proj_widgets)
        self.current_proj_btn = pidget
        self.current_proj_code = pidget.name
        os.environ["PROJECT_CODE"] = self.current_proj_code

        self.populateApps()
        self.buildProjSettings(self.current_proj_code)
        self.buildUserSettings()


    def editAppCmd(self,app_widget):
        if not os.path.isfile(app_widget.json_file):
            print "Not a valid app json: {}".format(app_widget.json_file)
            return

        app_data = ioUtils.jsonUtils.read(app_widget.json_file)
        if app_data is None:
            print "Corrupted app json: {}".format(app_widget.json_file)
            return

        try:
            appman_win = app_man.main(app_data)
        except Exception as err:
            print err
            print_exc()
            return

        self.populateApps()


    def killAppCmd(self,app_widget):
        if not os.path.isfile(app_widget.json_file):
            print "Not a valid app json: {}".format(app_widget.json_file)
            return

        if not qtUtils.dialogs.yesNo("Dashboard","Remove this app from list?"):
            return

        try:
            os.remove(app_widget.json_file)
        except Exception as err:
            print err
            print_exc()
            return

        self.populateApps()


    def appClickedCmd(self,app_widget):
        self.current_app_btn = app_widget
        app_path = os.path.abspath(app_widget.path)

        if not os.path.exists(app_path):
            return

        cmd_list = [app_path]

        if os.getenv("OS") not in ["nt"]:
            cmd_list = ["open",app_path]

        dummy_env = os.environ.copy()
        dummy_env.update(self.current_app_btn.environ)
        app_env = {}

        for de in dummy_env:
            app_env[str(de)] = str(dummy_env[de])

        try:
            print "Running: {}".format(app_path)
            subprocess.Popen(cmd_list, env = app_env, shell = False)
        except Exception as err:
            print_exc()
            print err


    def addAppCmd(self):
        try:
            appman_win = app_man.main(None)
        except Exception as err:
            print_exc
            print err

        self.populateApps()


    def archiveProjectCmd(self):
        if self.current_proj_btn is None:
            return

        if not qtUtils.dialogs.yesNo("Dashboard","Archive this project?"):
            return

        proj_dir = os.path.join(self.projects_root,self.current_proj_code)
        user_dir = os.path.join(os.environ["LOCAL_ROOT"],self.current_proj_code)

        for pdir in [proj_dir,user_dir]:
            if not ioUtils.dirUtils.archive(pdir,pdir,True):
                return

        self.all_proj_widgets.pop(self.all_proj_widgets.index(self.current_proj_btn))
        self.current_proj_btn.deleteLater()
        self.current_proj_btn = None

        self.selectFirstProject()


    def saveEditSettingsCmd(self):
        settings_grid = self.ui.settings_project_grid
        msg = "Create/Edit project?"

        #out_dir = os.path.join(self.defaults_root,os.getenv("OS"))
        out_dir = os.path.join(self.projects_root,
                               self.current_proj_code,
                               "config")

        if self.ui.settings_user_radio.isChecked():
            settings_grid = self.ui.settings_user_grid
            msg = "Edit current User Settings?"
            out_dir = os.path.join(os.environ["LOCAL_ROOT"],
                                   self.current_proj_code)
        elif self.ui.settings_project_radio2.isChecked():
            msg = "Create a New Project from these settings?"
            pcode = None

            for sets in ops.writeSettings(None,settings_grid):
                for sk in sets:
                    if sk["name"] == "project_code":
                        pcode = sk["value"]
                        break

            if type(pcode) is not str:
                return

            out_dir = os.path.join(self.projects_root,
                                   pcode,
                                   "config")

            user_dir = os.path.join(os.environ["LOCAL_ROOT"],pcode)

            if not ops.writeSettings(user_dir,self.ui.settings_user_grid):
                print "Failed to save user settings."

            thumb_tmp = os.path.join(self.defaults_root,
                                     "thumb.png")

            thumb_des = os.path.join(self.projects_root,
                                     pcode,
                                     "thumb.png")

            if not os.path.isfile(thumb_des):
                ioUtils.fileUtils.copy(thumb_tmp,thumb_des)

        if not qtUtils.dialogs.yesNo("Dashboard",msg):
            return

        if not ops.writeSettings(out_dir,settings_grid):
            print "Failed to save settings."

        if self.ui.settings_user_radio.isChecked():
            self.buildUserSettings()
        elif self.ui.settings_project_radio2.isChecked():
            self.initConfigs()


if __name__ == "__main__":
    app = run()
    app.exec_()
    sys.exit(0)