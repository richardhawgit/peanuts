import os
from pprint import pprint

from PySide2.QtWidgets import QLabel

from lib import qtUtils
from lib import ioUtils
from widgets import comboWidget as combo_wd
from widgets import textLineWidget as txt_wd
from widgets import tableWidgetColumns as columns_wd


def writeSettings(save_dir,gridw):
    out_settings = []
    out_count = 0

    for rw in range(gridw.rowCount()):
        gw1 = gridw.itemAtPosition(rw,0)
        gw2 = gridw.itemAtPosition(rw,1)

        if gw1 is None and gw2 is None:
            continue

        if gw1 is None and gw2 is not None:
            out_settings.append([])
            out_count+=1
            sep = gw2.widget()

            if type(sep) is QLabel:
                v_pattern = {"name":str(sep.text().lower().replace(" ","_")).rstrip(":"),
                             "value":None}
                out_settings[out_count-1].append(v_pattern)
            continue

        glabel = gw1.widget()
        gwidget = gw2.widget()
        gvalue = None
        gname = str(glabel.text().lower().replace(" ","_")).rstrip(":")
        v_pattern = {"name":gname}

        if type(gwidget) is txt_wd.main:
            v_pattern["value"] = str(gwidget.text())
        elif type(gwidget) is combo_wd.main:
            all_combos = []
            for cr in range(gwidget.count()):
                all_combos.append(str(gwidget.itemText(cr)))

            v_pattern["value"] = str(gwidget.currentText())
            v_pattern["predefined"] = all_combos
        elif type(gwidget) is columns_wd.main:
            v_pattern["value"] = qtUtils.table.itemsList(gwidget)

        out_settings[out_count-1].append(v_pattern)

    if save_dir is None:
        return out_settings

    jcount = 1
    for js in out_settings:
        jfile =  os.path.join(save_dir,"{}_{}.json".format(jcount,js[0]["name"]))

        if not ioUtils.jsonUtils.write(jfile,js):
            print "Failed to write: {}".format(jfile)
            pprint(js)
            return
        else:
            print "Saved: {}".format(jfile)
            jcount+=1

    return True