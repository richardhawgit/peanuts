import sys, os
from functools import partial

from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon
from PySide2.QtCore import QSize
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QGridLayout
from PySide2.QtWidgets import QHBoxLayout
from PySide2.QtWidgets import QSpacerItem
from PySide2.QtWidgets import QVBoxLayout
from PySide2.QtWidgets import QSizePolicy
from PySide2.QtWidgets import QToolButton
from PySide2.QtWidgets import QRadioButton
from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QStackedWidget

top_path = os.path.abspath(os.path.join(__file__,"../../.."))
if top_path not in sys.path:
    sys.path.append(top_path)

from widgets import manageWidget as mng_wd
from widgets import scrollWidget as scroll_wd
from widgets import optionsIconWidget as opt_wd
from widgets import settingsButtonWidget as setBtn_wd
from widgets import scrollFixedWidget as scrollFixed_wd

from lib import qtUtils


class build(QWidget):
    def __init__(self):
        super(build,self).__init__()

        self.tit = "Dashboard"
        self.winW = 320
        self.proj_w = 130
        self.ultimate = 16777215
        self.settings_label_style = "color:lightgrey; font-weight:bold; background-color: none"

        self.setWindowTitle(self.tit)
        self.setMinimumHeight(175)
        self.setMinimumWidth(self.winW)
        self.setStyleSheet("background-color: rgb(35,35,35);")

        self.task_button = None
        self.schedule_btn = None
        self.chat_btn = None
        self.gallery_btn = None
        self.docs_btn = None

        self.all_stacks = []
        self.options_widgets = []
        self.current_proj_btn = None

        self.createMainlayouts()
        self.createProjectsWidget()
        self.createOptionWidget()
        self.createMainWidgets()
        self.populateManage()
        self.arrangeLayouts()

        self.resize(self.winW,self.winW+150)
        self.setMinimumWidth(self.winW)
        self.resizeEvent = self.resizeWin


    def createMainlayouts(self):
        ikon = QIcon("logo.png")
        self.setWindowIcon(ikon)

        self.main_layout = QVBoxLayout(self)
        self.main_layout.setContentsMargins(5,0,5,30)
        self.main_layout.setSpacing(0)
        self.setLayout(self.main_layout)

        self.lower_widget = QWidget(self)
        self.lower_layout = QHBoxLayout(self.lower_widget)
        self.lower_layout.setContentsMargins(0,0,0,0)
        self.lower_layout.setSpacing(5)

        self.stack = QStackedWidget()


    def populateManage(self):
        #qtUtils.scrolls.add(self.apps_parent_widget,widget,cmd)
        tasks = ["tasks","schedule","chat","gallery","docs"]
        all_task_btn = []

        for task in tasks:
            t_icon = os.path.join(os.path.dirname(__file__),
                                  "icons",
                                  "{}.png".format(task))

            t_wd = mng_wd.main(task,t_icon)
            qtUtils.scrolls.add(self.manage_widget,t_wd)
            all_task_btn.append(t_wd)

        [self.task_button,self.schedule_btn,self.chat_btn,self.gallery_btn,self.docs_btn] = all_task_btn


    def createOptionWidget(self):
        self.options_layout = QHBoxLayout()
        self.options_layout.setSpacing(10)
        self.options_layout.setContentsMargins(2,7,7,5)

        self.options_widget = QWidget(self)
        self.options_widget.setLayout(self.options_layout)

        icon_map = [{"text":"Apps","icon":"apps.png"},
                    {"text":"Manage","icon":"manage.png"},
                    {"text":"Settings","icon":"settings.png"}]

        for ic in icon_map:
            icon_path = os.path.join(os.path.dirname(__file__),"icons",ic["icon"])
            ikon = opt_wd.main(ic["text"],icon_path)
            self.options_layout.addWidget(ikon)
            self.options_widgets.append(ikon)

        self.user_label = QLabel("DefaultUser")
        self.user_label.setStyleSheet("font-size: 20px; color: grey")
        self.user_label.setAlignment(Qt.AlignCenter)
        self.user_label.setFixedHeight(40)
        self.options_layout.addWidget(self.user_label)
        self.options_layout.setStretch(3,1)


    def createProjectsWidget(self):
        proj_widget = scrollFixed_wd.main()

        self.proj_scroll = proj_widget.scroll
        self.proj_scroll.setMinimumSize(self.proj_w+2,0)
        self.proj_scroll.setMaximumSize(self.proj_w+2,16777215)

        self.proj_layout = QVBoxLayout()
        self.proj_layout.setSpacing(20)
        self.proj_layout.setMargin(0)
        proj_widget.layout.addLayout(self.proj_layout)


    def createMainWidgets(self):
        # apps
        self.apps_layout = QHBoxLayout()
        self.apps_layout.setContentsMargins(0,0,0,0)

        self.apps_widget = QWidget()
        self.apps_widget.setLayout(self.apps_layout)

        # manage
        self.manage_widget = scroll_wd.main()

        # user
        self.settings_user_widget = scrollFixed_wd.main()
        self.settings_user_widget.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.settings_user_grid = QGridLayout()
        self.settings_user_grid.setContentsMargins(0,1,10,1)

        settings_user_spacer = QSpacerItem(1,
                                           self.ultimate,
                                           QSizePolicy.Minimum,
                                           QSizePolicy.Expanding)

        self.settings_user_widget.layout.addLayout(self.settings_user_grid)
        self.settings_user_widget.layout.addItem(settings_user_spacer)

        # project
        self.settings_project_widget = scrollFixed_wd.main()
        self.settings_project_widget.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.settings_project_grid = QGridLayout()
        self.settings_project_grid.setContentsMargins(0,1,10,1)

        settings_project_spacer = QSpacerItem(1,
                                              self.ultimate,
                                              QSizePolicy.
                                              Minimum,
                                              QSizePolicy.Expanding)

        self.settings_project_widget.layout.addLayout(self.settings_project_grid)
        self.settings_project_widget.layout.addItem(settings_project_spacer)

        # options
        self.settings_user_radio = QRadioButton("User")
        self.settings_user_radio.setStyleSheet(self.settings_label_style)

        self.settings_project_radio1 = QRadioButton("Project (Current)")
        self.settings_project_radio1.setStyleSheet(self.settings_label_style)

        self.settings_project_radio2 = QRadioButton("Project (New)")
        self.settings_project_radio2.setStyleSheet(self.settings_label_style)

        settings_spacer = QSpacerItem(self.ultimate,
                                      1,
                                      QSizePolicy.Expanding,
                                      QSizePolicy.Minimum)

        settings_kill_icon = os.path.join(os.path.dirname(__file__),"icons","delete.png")
        self.settings_kill_btn = setBtn_wd.main("Archive",
                                                settings_kill_icon,
                                                "slategrey")
        self.settings_kill_btn.hide()

        settings_save_icon = os.path.join(os.path.dirname(__file__),"icons","save.png")
        self.settings_save_btn = setBtn_wd.main("Save",
                                                settings_save_icon,
                                                "slategrey")

        # assemble
        self.settings_stack = QStackedWidget()
        self.settings_stack.addWidget(self.settings_user_widget.scroll)
        self.settings_stack.addWidget(self.settings_project_widget.scroll)
        self.settings_stack.setStyleSheet("background-color:rgb(35,35,35);")

        settings_top_layout = QHBoxLayout()
        settings_top_layout.addWidget(self.settings_user_radio)
        settings_top_layout.addWidget(self.settings_project_radio1)
        settings_top_layout.addWidget(self.settings_project_radio2)
        settings_top_layout.addItem(settings_spacer)
        settings_top_layout.addWidget(self.settings_kill_btn)
        settings_top_layout.addWidget(self.settings_save_btn)

        self.settings_widget = QWidget()
        settings_layout = QVBoxLayout()
        self.settings_widget.setLayout(settings_layout)
        settings_layout.addLayout(settings_top_layout)
        settings_layout.addWidget(self.settings_stack)

        self.settings_user_radio.setChecked(True)


    def arrangeLayouts(self):
        self.main_layout.addWidget(self.options_widget)
        self.main_layout.addWidget(self.lower_widget)

        self.lower_layout.addWidget(self.proj_scroll)
        self.lower_layout.addWidget(self.stack)

        self.stack.addWidget(self.apps_widget)
        self.stack.addWidget(self.manage_widget)
        self.stack.addWidget(self.settings_widget)

        self.all_stacks = [self.apps_widget,
                           self.manage_widget,
                           self.settings_widget]


    def resizeWin(self,event):
        i_width = 40
        i_style = Qt.ToolButtonIconOnly

        if self.width() > 550:
            i_width = 120
            i_style = Qt.ToolButtonTextBesideIcon

        for top_ic in self.options_widgets:
            top_ic.setToolButtonStyle(i_style)
            top_ic.setFixedSize(i_width,40)
            top_ic.setIconSize(QSize(i_width,40))
            top_ic.repaint()


if __name__ == "__main__":
    top_app = QApplication(sys.argv)
    ui = build()
    ui.show()

    top_app.exec_()
    sys.exit(0)